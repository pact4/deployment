from flask import Flask, render_template, request, session, redirect, url_for, g
import sqlite3

import dash
from dash import dcc
from dash import html
from flask_login.utils import login_required
import plotly.express as px
import pandas as pd
from flask.helpers import get_root_path
import dash 
from dash import dcc
from dash import html 
from dash.dependencies import Input, Output, State
import dash_bootstrap_components as dbc

import base64
import io

import numpy as np
import os
import sys
sys.path.append('./Code/Feature_Implementation')
sys.path.append('./Code/Algorithms')
sys.path.append('./Code/dataset')
from total_num_beats import total_num_beats
from PVCs_hora import total_num_pvcs
from avg_beats import avg_beats
from PVCs_hora import PVCs_hour
from read_mat_file import mat_tolists
import joblib
from windowed_ecg import window_ecg
from extract_features import features
from teste import ecg_plot
algorithm_path = os.path.abspath('./Code/Algorithms/Random_Forest.joblib')
model=joblib.load(algorithm_path)

# Initialize Flask App
app = Flask(__name__)
app.secret_key = 'your_secret_key_here'

DATABASE = 'users.db'

# Function to get a connection to the database
def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
    return db

# Function to close the database connection
@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()

# Create a table for users
def init_db():
    with app.app_context():
        db = get_db()
        db.execute('''CREATE TABLE IF NOT EXISTS users
                     (username TEXT PRIMARY KEY NOT NULL,
                     password TEXT NOT NULL);''')
        db.commit()

# Initialize the database
init_db()

# Function to check if a user exists in the database
def not_user(username, password):
    db = get_db()
    cursor = db.execute("SELECT * FROM users WHERE username=? AND password=?", (username, password))
    row = cursor.fetchone()
    if row is not None:
        return False
    else:
        return True

# Function to add a new user to the database
def add_user(username, password):
    if not_user(username,password):
        db = get_db()
        db.execute("INSERT INTO users (username, password) VALUES (?, ?)", (username, password))
        db.commit()

# Function to check if a user exists in the database 
def check_user(username, password):
    db = get_db()
    cursor = db.execute("SELECT * FROM users WHERE username=? AND password=?", (username, password))
    row = cursor.fetchone()
    if row is not None:
        return True
    else:
        return False

logged_in = False

# Route for the login page
@app.route('/', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        add_user("admin","admin")
        username = request.form['username']
        password = request.form['password']
        if check_user(username, password):
            session['username'] = username
            global logged_in
            logged_in=True
            return redirect(url_for('dash'))
        else:
            return render_template('login.html', error_message='Nome de Utilizador ou Palavra-Passe inválida')
    else:
        return render_template('login.html')

# Route for the logout page
@app.route('/logout')
def logout():
    session.pop('username', None)
    global logged_in
    logged_in=False
    return redirect(url_for('login'))

# Dash stylesheet
external_stylesheets = ['./static/bootstrap.css']

# Initialize dash
dash_app = dash.Dash(external_stylesheets=external_stylesheets,server=app, name="Dashboard", url_base_pathname="/dash/")

# Dash title
dash_app.title="PACT-PVC Detection"

# Definition of Navbar
navbar = dbc.Navbar(
    [
        html.A(
            dbc.Row([
                    dbc.Col(html.H1('PACT'),md=4),
                    dbc.Col([
                # Logout button
                dbc.Button("Logout",id='logout-button',color='secondary',style={"line-height":"20px","padding-left":"20px","padding-right":"20px","margin-left":"934px"}),
                html.Div(id='logout-redirect'),
            ], md=8, style={"align": "right","margin-top":"10px"})
                ],
            ),
        )
    ],
    color= 'rgba(90, 88, 215,0.8)',
    sticky="top",
    

)

# Layout
dash_app.layout = dbc.Container(

    # Navbar
    dbc.Row([
        dbc.Col([
            html.Div([
            navbar
        ],style={"width":"112%"}),
        ]),

        # Button upload and analyse
        dbc.Row([
            dbc.Col([
            dbc.Button(dcc.Upload(
            id='upload-data',
            children=html.Div([
                'Upload ',
            ],),
            multiple=False # Allow multiple files to be uploaded
        ),color='secondary',style={'margin:': '10px',"padding-left":"20px","padding-right":"20px","margin-left":"70px"})
    ],md=2,style={"align":'left'}),
            dbc.Col([
                html.Div(id='output-data-upload',style={"width":"80%","padding-left":"280px","padding-top":"20px","line-height":"10px"})
            ],md=8,style={"align":"center"}),

            dbc.Col([
                dbc.Button('Analisar', id='analyze-button',color='secondary',style={"marginLeft":"80px","padding-left":"20px","padding-right":"20px"}),
                dcc.Store(id='dados'),
            ],md=2,style={"align":"right"})
        ]),
        # Card 1 and 2
        dbc.Row([
                # Card 1 (Graph and slider)
                dbc.Col([
                    dbc.Card([
                        dbc.CardBody([
                            html.H5("ECG Graph",style={"color": "#000000"}),
                            html.Div(id='ecg-figure'),
                            html.Div(id='container-id',children=[
                                dcc.Slider(
                                    id='slider',
                                    min=1,
                                    max=30,
                                    step=1,
                                    value=1),
                                ], style={'display': 'none'}),
                    ])
                    ], color="light", outline=True, style={"height":"700px", "margin-top": "10px",
                                                        "box-shadow": "0 4px 4px 0 rgba(0,0,0.15), 0 4px 20px 0 rgba(0, 0, 0, 0.19}",
                                                        'background-color': 'rgba(255, 255, 255, 0.8)','border-color': 'transparent','opacity': 0.9})
                ],md=8,style={"align":"left","padding-left":"70px"}),
                # Card 2 (Information about the patient's ecg)
                dbc.Col([
                    dbc.Card([
                    dbc.CardBody([
                            html.H6("Numero Total de Batimentos",style={"color": "#000000"}),
                            html.H3("-",style={"color": "#5A58D7"}, id="num-total-batimentos"),
                            html.H6("PVC/h",style={"color": "#000000"}),
                            html.H3("-",style={"color": "#5A58D7"}, id="num-pvc-h"),
                            html.H6("Numero Total de PVCs",style={"color": "#000000"}),
                            html.H3("-",style={"color": "#5A58D7"}, id="num-total-pvc"),
                            html.H6("Batimentos por minuto (bpm)",style={"color": "#000000"}),
                            html.H3("-",style={"color": "#5A58D7"}, id="num-batimento-medio"),
                        ])
                    ], color="light", outline=True, style={"margin-top": "10px",
                                                        "box-shadow": "0 4px 4px 0 rgba(0,0,0.15), 0 4px 20px 0 rgba(0, 0, 0, 0.19}",
                                                        'background-color': 'rgba(255, 255, 255, 0.8)','border-color': 'transparent','opacity': 0.8})
                ], md=4,style={"padding-right":"-50px","align":"right"}),
    ])
    ]),style={"margin":0,"width":"100%"}
    )

# Logout interaction
@dash_app.callback(Output('logout-redirect', 'children'),
                  Input('logout-button', 'n_clicks'))
def logout(n_clicks):
    if n_clicks is not None and n_clicks > 0:
        # Clear the session data
        session.clear()
        # Redirect the user to the login page
        return dcc.Location(pathname='/logout', id='logout-redirect-url')
    else:
        return None
    
# Upload and Analyse interactions
def parse_contents(contents, filename):
    content_type, content_string = contents.split(',')

    decoded = base64.b64decode(content_string)
    try:
        if '.mat' in filename:
            # Assuming the file is a .mat file
            ecg, r, pvc = mat_tolists(io.BytesIO(decoded))
            return dbc.Alert('Arquivo carregado com sucesso!', color="success",className="text-center",style={'align':'center'})
        else:
            return dbc.Alert('Oops! O arquivo selecionado deve ser .mat', color="primary",className="text-center",style={'align':'center'})
    except Exception as e:
        print(e)
        return dbc.Alert('Oops! Ocorreu um erro ao processar este arquivo.', color="primary",className="text-center",style={'align':'center'})


@dash_app.callback(
    [Output('dados','data'),
    Output('output-data-upload', 'children'),
    Output('num-total-batimentos', 'children'),
    Output('num-pvc-h', 'children'),
    Output('num-total-pvc', 'children'),
    Output('num-batimento-medio', 'children')],
    [Input('upload-data', 'contents'),
    Input('upload-data', 'filename'),
    Input('analyze-button', 'n_clicks')],
    [State('output-data-upload', 'children')])

def update_card(contents, filename, n_clicks, upload):
    ctx = dash.callback_context
    if not ctx.triggered:
        return dash.no_update
    else:
        button_id = ctx.triggered[0]['prop_id'].split('.')[0]
        if button_id == 'upload-data':
            if contents is not None:
                return None, \
                    parse_contents(contents, filename), \
                    dash.no_update, \
                    dash.no_update, \
                    dash.no_update, \
                    dash.no_update
            else:
                return None, \
                    dash.no_update, \
                    dash.no_update, \
                    dash.no_update, \
                    dash.no_update, \
                    dash.no_update
        elif button_id == 'analyze-button':
            try:
                if contents is not None:
                    if upload['props']['children'] == "Arquivo carregado com sucesso!":
                        content_type, content_string = contents.split(',')
                        decoded = base64.b64decode(content_string)
                        ecg, r, pvc = mat_tolists(io.BytesIO(decoded))
                        data=[ecg,r,pvc]
                        ecg_window=window_ecg(ecg,r)
                        ftrs = features(ecg_window, r)
                        total_batimentos = total_num_beats(r)
                        predicted_pvc=model.predict(ftrs)
                        total_pvc = total_num_pvcs(predicted_pvc)
                        pvc_hora = PVCs_hour(total_pvc)
                        batimento_medio = avg_beats(r)
                        return data, \
                            None, \
                            total_batimentos, \
                            pvc_hora, \
                            total_pvc, \
                            batimento_medio
                    else:
                        return None, \
                            dbc.Alert('Oops! Ocorreu um erro com o arquivo carregado.', color="primary",className="text-center",style={'align':'center'}), \
                            dash.no_update, \
                            dash.no_update, \
                            dash.no_update, \
                            dash.no_update
                else:
                    return None, \
                            dbc.Alert('Oops! Nenhum arquivo foi carregado.', color="primary",className="text-center",style={'align':'center'}), \
                            dash.no_update, \
                            dash.no_update, \
                            dash.no_update, \
                            dash.no_update
            except Exception as e:
                print(e)
                return None, \
                        dbc.Alert('Oops! Ocorreu um erro ao analisar este arquivo.', color="primary",className="text-center",style={'align':'center'}), \
                        dash.no_update, \
                        dash.no_update, \
                        dash.no_update, \
                        dash.no_update

# Graph interaction
@dash_app.callback(
    Output('ecg-figure', 'children'),
    [Input('slider', 'value'),
    Input('dados', 'data')]
)
def update_graph(slider_value,data):
    if data is not None:
        # Update the figure based on the slider value
        ecg,r,pvc=data[0],data[1],data[2]
        ecg_window=window_ecg(ecg,r)
        ftrs = features(ecg_window, r)
        predicted_pvc=model.predict(ftrs)
        updated_fig = ecg_plot(ecg, r, predicted_pvc,slider_value)
        updated_fig.update_layout(margin= {'l': 0, 'r': 0, 't': 0, 'b': 0}, legend=dict(orientation='h',yanchor='bottom'))
        return dcc.Graph(figure=updated_fig,style={'height':"595px","width":"700px","marginLeft":"70px","color": "rgba(255,255,255,0.7)"})

# Slider interaction
@dash_app.callback(
    Output('container-id','style'),
    [Input('ecg-figure','children')]
)
def show_slider(figure):
    if figure is not None:
        return {'display':'inline'}
    else:
        return {'display':'none'}
    


# Route for the dash page (requires login)
@app.route('/dash')
def dash():
    if logged_in==True:
        return dash_app.index()
    else:
        return redirect(url_for('login'))


if __name__ == '__main__':
    app.run(debug=True)
